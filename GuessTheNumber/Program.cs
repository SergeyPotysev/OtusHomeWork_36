﻿using GuessTheNumber.Data;
using GuessTheNumber.IO;
using GuessTheNumber.Services;

namespace GuessTheNumber
{
    // Ответственность: запуск программы
    class Program
    {
        static void Main()
        {
            DataRepo data = new SetData().Get();
            IReadWrite device = new FileIO();
            new Guessing(data, device, new Action(data, device)).Run();
        }

    }
}
