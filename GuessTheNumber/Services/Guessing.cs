﻿using GuessTheNumber.Data;
using GuessTheNumber.IO;

namespace GuessTheNumber.Services
{
    /// <summary>
    /// Ответственность: организация процесса игры
    /// </summary>
    class Guessing
    {
        private static DataRepo _data;
        private IAction _actions;
        private IReadWrite _device;

    public Guessing(DataRepo data, IReadWrite device, IAction actions)
        {
            _data = data;
            _actions = actions;
            _device = device;
        }

        public void Run()
        {
            _device.Writer($"Угадайте число в диапазоне: {_data.MinRangeNumber} - {_data.MaxRangeNumber}");
            _device.Writer("Введите число или 0 для выхода: ");
            int attempt = 0;
            while (!_data.NeedToGoOut && attempt < _data.AttemptsTotal)
            {
                _device.Writer($"Осталось попыток: {_data.AttemptsTotal - attempt++}");

                // Получить новое число
                _data.TryNumber = _device.Reader();

                // Выяснить вопрос: что делать дальше?
                string method = CompareData.Result(_data);

                // Выполнить нужное действие
                _actions.ActionChoice(method);
            }

            // Результат игры
            if (_data.NumberGuessed)
                _device.Writer("\nВы отгадали секретное число!!!");
            else
                _device.Writer("\nВы не отгадали секретное число");

            // Показать секретное число
            _device.Writer($"\nСекретное число: {_data.HiddenNumber}");
        }

    }
}
