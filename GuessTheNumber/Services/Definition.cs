﻿namespace GuessTheNumber.Services
{
    class Definition
    {
        public enum Terms
        {
            /// <summary>
            /// Введенное число больше, чем секретное.
            /// </summary>
            MoreThanSecret,

            /// <summary>
            /// Введенное число меньше, чем секретное.
            /// </summary>
            LessThanSecret,

            /// <summary>
            /// Введенное число вне диапазона.
            /// </summary>
            NumberOutOfRange,

            /// <summary>
            /// Число отгадано.
            /// </summary>
            NumberGuessed,

            /// <summary>
            /// Выход из игры.
            /// </summary>
            NeedToGoOut,

            /// <summary>
            /// Метод для дальнейшего расширения функционала.
            /// </summary>
            AnotherAction
        }
    }
}
