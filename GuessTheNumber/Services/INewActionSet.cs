﻿namespace GuessTheNumber.Services
{
    interface INewActionSet : IAction
    {
        void NewAction();
    }
}
