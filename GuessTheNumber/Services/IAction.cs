﻿namespace GuessTheNumber.Services
{
    /// <summary>
    /// Выполнение действий с рабочими переменными игры
    /// </summary>
    public interface IAction
    {
        /// <summary>
        /// Выполнить нужное действие.
        /// </summary>
        void ActionChoice(string method);

        /// <summary>
        /// Введенное число больше, чем секретное.
        /// </summary>
        void MoreThanSecret();

        /// <summary>
        /// Введенное число меньше, чем секретное.
        /// </summary>
        void LessThanSecret();

        /// <summary>
        /// Введенное число вне диапазона.
        /// </summary>
        void NumberOutOfRange();

        /// <summary>
        /// Число отгадано.
        /// </summary>
        void NumberGuessed();

        /// <summary>
        /// Выход из игры.
        /// </summary>
        void NeedToGoOut();

        /// <summary>
        /// Метод для дальнейшего расширения функционала.
        /// </summary>
        void AnotherAction();
    }
}