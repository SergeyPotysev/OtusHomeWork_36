﻿using GuessTheNumber.Data;
using GuessTheNumber.IO;
using System.Collections.Generic;
using static GuessTheNumber.Services.Definition;

namespace GuessTheNumber.Services
{
    /// <summary>
    /// Ответственность: выполнение действий с рабочими переменными игры
    /// </summary>
    class Action : IAction
    {
        private DataRepo _data;
        private IReadWrite _device;
        private delegate void ActionDelegate();
        private Dictionary<string, ActionDelegate> toDo;

        public Action(DataRepo data, IReadWrite device)
        {
            _data = data;
            _device = device;
            toDo = new Dictionary<string, ActionDelegate>
            {
                { nameof(Terms.MoreThanSecret), MoreThanSecret },
                { nameof(Terms.LessThanSecret), LessThanSecret },
                { nameof(Terms.NumberOutOfRange), NumberOutOfRange },
                { nameof(Terms.NumberGuessed), NumberGuessed },
                { nameof(Terms.NeedToGoOut), NeedToGoOut },
                { nameof(Terms.AnotherAction), AnotherAction }
            };
        }


        public void ActionChoice(string method)
        {
            toDo[method].Invoke();
        }


        public void MoreThanSecret()
        {
            _device.Writer("\nСекретное число МЕНЬШЕ указанного!");
            ChangeData.SetValue(ref _data.MaxRangeNumber, _data.TryNumber);
            _device.Writer($"Угадайте число в диапазоне: {_data.MinRangeNumber} - {_data.MaxRangeNumber}");
        }


        public void LessThanSecret()
        {
            _device.Writer("\nСекретное число БОЛЬШЕ указанного!");
            ChangeData.SetValue(ref _data.MinRangeNumber, _data.TryNumber);
            _device.Writer($"Угадайте число в диапазоне: {_data.MinRangeNumber} - {_data.MaxRangeNumber}");
        }


        public void NumberOutOfRange()
        {
            _device.Writer("Указанное число вне диапазона!");
        }


        public void NumberGuessed()
        {

            _data.NumberGuessed = true;
            NeedToGoOut();
        }


        public void NeedToGoOut()
        {
            _data.NeedToGoOut = true;
        }


        public void AnotherAction()
        {
        }

    }
}
