﻿namespace GuessTheNumber.IO
{
    /// <summary>
    /// Тип данных для устройства с возможностью ввода и вывода данных.
    /// </summary>
    interface IReadWrite : IWriter, IReader
    {
    }
}
