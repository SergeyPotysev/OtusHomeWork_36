﻿using System.IO;

namespace GuessTheNumber.IO
{
    class FileIO : ConsoleIO
    {
        // Вывод строки на консоль и в файл.
        public override void Writer(string text)
        {
            base.Writer(text);

            string filePath = "GuessTheNumber.log";
            using StreamWriter sw = new StreamWriter(filePath, true, System.Text.Encoding.Default);
            sw.WriteLine(text);
        }
    }
}
