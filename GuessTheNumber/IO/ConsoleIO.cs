﻿using System;
using System.Linq;

namespace GuessTheNumber.IO
{
    public class ConsoleIO : IReadWrite
    {
        // Вывод строки на консоль.
        public virtual void Writer(string text)
        {
            Console.WriteLine(text);
        }


        // Чтение строки с консоли.
        public virtual int Reader()
        {
            string stroka, digits;
            int result;
            while (true)
            {
                stroka = Console.ReadLine();
                digits = string.Join("", stroka.Where(c => char.IsDigit(c)));
                if (digits.Length > 0)
                {
                    int.TryParse(digits, out result);
                    break;
                }
            }
            return result;
        }

    }
}
