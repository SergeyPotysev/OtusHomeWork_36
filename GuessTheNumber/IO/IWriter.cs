﻿namespace GuessTheNumber.IO
{
    public interface IWriter
    {
        /// <summary>
        /// Вывод данных на указанное устройство.
        /// </summary>
        void Writer(string text);
    }
}