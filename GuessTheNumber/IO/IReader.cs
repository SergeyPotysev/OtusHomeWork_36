﻿namespace GuessTheNumber.IO
{
    public interface IReader
    {
        /// <summary>
        /// Чтение строки с устройства ввода данных.
        /// </summary>
        int Reader();
    }
}