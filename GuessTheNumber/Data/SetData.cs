﻿using System;

namespace GuessTheNumber.Data
{
    /// <summary>
    /// Ответственность: инициализация рабочих переменных игры
    /// </summary>    
    public class SetData
    {
        private DataRepo _data;
        public SetData()
        {
            _data = new DataRepo();
            var settings = new ReadSettings();
            _data.MaxRangeNumber = settings.MaxRangeNumber;
            _data.MinRangeNumber = settings.MinRangeNumber;
            _data.AttemptsTotal = settings.AttemptsTotal;
            _data.HiddenNumber = new Random().Next(_data.MinRangeNumber, _data.MaxRangeNumber);
            _data.NeedToGoOut = false;
            _data.NumberGuessed = false;
        }
        public DataRepo Get()
        {
            return _data;
        }
    }
}
