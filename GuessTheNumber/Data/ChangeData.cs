﻿namespace GuessTheNumber.Data
{
    /// <summary>
    /// Ответственность: изменение рабочих переменных игры
    /// </summary>
    class ChangeData
    {
        public static void SetValue(ref int parameter, int newValue)
        {
            parameter = newValue;
        }
    }
}
