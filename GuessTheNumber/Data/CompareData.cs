﻿using static GuessTheNumber.Services.Definition;

namespace GuessTheNumber.Data
{
    /// <summary>
    /// Ответственность: сравнение рабочих переменных игры
    /// </summary>
    public class CompareData
    {
        public static string Result(DataRepo data)
        {
            if (data.TryNumber == 0)
                return nameof(Terms.NeedToGoOut);

            if (data.TryNumber <= data.MinRangeNumber || data.TryNumber >= data.MaxRangeNumber)
            {
                return nameof(Terms.NumberOutOfRange);
            }

            if (data.HiddenNumber > data.TryNumber)
            {
                return nameof(Terms.LessThanSecret);
            }

            if (data.HiddenNumber < data.TryNumber)
            {
                return nameof(Terms.MoreThanSecret);
            }

            if (data.HiddenNumber == data.TryNumber)
            {
                return nameof(Terms.NumberGuessed);
            }

            return nameof(Terms.AnotherAction);
        }

    }
}
