﻿namespace GuessTheNumber.Data
{
    /// <summary>
    ///  Ответственность: хранение рабочих переменных игры
    /// </summary>
    public class DataRepo
    {
        // Случайное число для отгадывания
        public int HiddenNumber { get; set; }

        // Число текущей попытки
        public int TryNumber { get; set; }

        // Текущая верхняя граница диапазона
        public int MaxRangeNumber;

        // Текущая нижняя граница диапазона
        public int MinRangeNumber;

        // Всего попыток для отгадывания
        public int AttemptsTotal { get; set; }

        // Признак выхода из игры
        public bool NeedToGoOut { get; set; }

        // Признак угаданного числа
        public bool NumberGuessed { get; set; }

    }
}
