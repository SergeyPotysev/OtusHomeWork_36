﻿using Microsoft.Extensions.Configuration;
using System;

namespace GuessTheNumber.Data
{
    /// <summary>
    /// Ответственность: чтение настроек из файла appsettings.json
    /// </summary>
    public class ReadSettings
    {
        /// <summary>
        /// Количество попыток отгадывания.
        /// </summary>
        public int AttemptsTotal { get; set; }

        /// <summary>
        /// Минимальное число диапазона чисел для отгадывания
        /// </summary>
        public int MinRangeNumber { get; set; }

        /// <summary>
        /// Максимальное число диапазона чисел для отгадывания
        /// </summary>
        public int MaxRangeNumber { get; set; }

        public ReadSettings()
        {
            IConfiguration AppConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            AttemptsTotal = Convert.ToInt32(AppConfiguration.GetSection("AttemptsTotal").Value);
            MinRangeNumber = Convert.ToInt32(AppConfiguration.GetSection("MinRangeNumber").Value);
            MaxRangeNumber = Convert.ToInt32(AppConfiguration.GetSection("MaxRangeNumber").Value);
        }

    }
}
